package com.leap.shoppingcar.Interface;

import com.leap.shoppingcar.Model.objToken;

import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface TokenService {
    public abstract HttpEntity<objToken> getToken(@RequestBody objToken token);
}