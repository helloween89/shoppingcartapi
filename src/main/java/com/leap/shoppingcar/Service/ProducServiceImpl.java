package com.leap.shoppingcar.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.leap.shoppingcar.Interface.ProducService;
import com.leap.shoppingcar.Interface.prodInterSearch;
import com.leap.shoppingcar.Model.Product.Product;
import com.leap.shoppingcar.Model.Product.especificaciones;
import com.leap.shoppingcar.Model.Product.objPromociones;
import com.leap.shoppingcar.Model.Product.objVigencia;
import com.leap.shoppingcar.Model.ProductDB.brand;
import com.leap.shoppingcar.Model.ProductDB.category;
import com.leap.shoppingcar.Model.ProductDB.detail_product;
import com.leap.shoppingcar.Model.ProductDB.product;
import com.leap.shoppingcar.Model.ProductDB.product_Promo;
import com.leap.shoppingcar.Model.ProductDB.product_Spec;
import com.leap.shoppingcar.Model.ProductDB.subcat;
import com.leap.shoppingcar.Model.ProductDTO.prodDetail;
import com.leap.shoppingcar.Model.ProductDTO.prodSpec;
import com.leap.shoppingcar.Repository.ProductDB.brandRepository;
import com.leap.shoppingcar.Repository.ProductDB.catRepository;
import com.leap.shoppingcar.Repository.ProductDB.detail_productRepository;
import com.leap.shoppingcar.Repository.ProductDB.productRepository;
import com.leap.shoppingcar.Repository.ProductDB.product_PromoRepository;
import com.leap.shoppingcar.Repository.ProductDB.product_SpecRepository;
import com.leap.shoppingcar.Repository.ProductDB.subcatRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProducServiceImpl implements ProducService {

    @Autowired
    private catRepository catrepo;

    @Autowired
    private subcatRepository subrepository;

    @Autowired
    private productRepository prodrepository;

    @Autowired
    private detail_productRepository detail_prod_repo;

    @Autowired
    private product_SpecRepository prod_spec_repo;

    @Autowired
    private product_PromoRepository prod_promo_repo;

    @Autowired
    private brandRepository brandrepo;

    private category cat;
    private subcat subCat;
    private product objprod;
    private detail_product detail_prod;
    private product_Spec prod_spec;
    private product_Promo prod_promo;
    private brand brandprod;
    private Map<Integer, especificaciones> hspec;
    private Map<Integer, objPromociones> hpromotion;

    @Override
    public void getProductinfo() throws JsonParseException, JsonMappingException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();

        // ArrayList<Product> product = objectMapper.readValue(new
        // File("/home/helloween/Documents/Proyectos/productos.json"),objectMapper.getTypeFactory().constructCollectionType(List.class,
        // Product.class));
        ArrayList<Product> product = objectMapper.readValue(new File("/home/leap01/productos.json"),
                objectMapper.getTypeFactory().constructCollectionType(List.class, Product.class));

        hspec = new HashMap<>();
        hpromotion = new HashMap<>();

        for (Product prod : product) {

            cat = null;
            subCat = null;
            objprod = null;
            detail_prod = null;
            prod_spec = null;
            prod_promo = null;
            brandprod = null;

            // Get the specification list
            if (prod.getEspecificaciones() != null) {
                int key = 0;
                hspec.clear();
                for (especificaciones specs : prod.getEspecificaciones()) {
                    hspec.put(key, new especificaciones(specs.getTipo(), specs.getValor()));
                    key++;
                }

            }

            // Get the promotions list
            if (prod.getPromociones() != null) {
                int key = 0;
                hpromotion.clear();
                for (objPromociones promotions : prod.getPromociones()) {
                    hpromotion.put(key, new objPromociones(promotions.getTipo(), promotions.getPromocion(),
                            new objVigencia(promotions.getVigencia().getInicio(), promotions.getVigencia().getFin())));
                    key++;
                }
            }

            cat = new category(prod.getIdCategoria(), prod.getCategoria());
            catrepo.save(cat);

            subCat = new subcat(prod.getIdSubCategoria(), prod.getSubcategoria(), cat);
            subrepository.save(subCat);

            brandprod = new brand(prod.getIdMarca(), prod.getMarca());
            brandrepo.save(brandprod);

            objprod = new product(prod.getIdProducto(), prod.getClave(), prod.getNombre(), cat, subCat, brandprod);
            prodrepository.save(objprod);

            detail_prod = new detail_product(prod.getActivo(), prod.getPrecio(), prod.getMoneda(), prod.getImagen(),
                    objprod);
            detail_prod_repo.save(detail_prod);

            for (Map.Entry<Integer, especificaciones> entry : hspec.entrySet()) {
                String type = entry.getValue().getTipo();
                String value = entry.getValue().getValor();
                prod_spec = new product_Spec(type, value, detail_prod);
                prod_spec_repo.save(prod_spec);
            }

            for (Map.Entry<Integer, objPromociones> entry : hpromotion.entrySet()) {
                String type = entry.getValue().getTipo();
                double value = entry.getValue().getPromocion();
                String vad_start = entry.getValue().getVigencia().getInicio();
                String vad_end = entry.getValue().getVigencia().getFin();
                prod_promo = new product_Promo(type, value, vad_start, vad_end, detail_prod);
                prod_promo_repo.save(prod_promo);
            }

            // hspec.forEach((k,v)->System.out.println("Key : " + k + " Value : " + v));

        }

        /*
         * category cat = new category(28500, "Tarjetas de video"); catrepo.save(cat);
         * subcat sub = new subcat(1600, "NVIDIA",cat); subrepository.save(sub); brand
         * brandprod = new brand(10, "Logitech"); brandrepo.save(brandprod); product
         * prod = new product(34600,"CLV005", "Tarjeta GTX 3000", cat, sub, brandprod);
         * prodrepository.save(prod); detail_product detail_prod = new detail_product(1,
         * 200.80,"USD","http://www.domain.com/image.jpg",prod);
         * detail_prod_repo.save(detail_prod); prod_spec = new
         * product_Spec("pruebatipo","Valor de Prueba", detail_prod);
         * prod_spec_repo.save(prod_spec); prod_promo = new product_Promo("pruebatipo",
         * 4.82, "2020-09-04T07:00:00.000Z", "2020-10-01T07:00:00.000Z", detail_prod);
         * prod_promo_repo.save(prod_promo);
         */

    }

    @Override
    public List<prodInterSearch> getSearchProd(String search) {
        List<prodInterSearch> psearch = prodrepository.findsearchprod(search);
        /*
         * for (prodInterSearch prod : psearch) { System.out.println("Producto " +
         * prod.getName() + " " + "Clave " + prod.getClave() + " " + "Precio " +
         * prod.getPrice() + " " + "Moneda " + prod.getCurrency() + " Imagen " +
         * prod.getImage()); }
         */
        return psearch;
    }

    @Override
    public prodDetail getprod(int idprod) {
        prodDetail product = prodrepository.findprod(idprod);
        System.out.println("Clave Prod" + product.getClave());
        return product;
    }

    @Override
    public List<prodSpec> getprodSpecs(int id) {
        List<prodSpec> prodspec = prodrepository.finprodspec(id);
        return prodspec;
    }
    
}
