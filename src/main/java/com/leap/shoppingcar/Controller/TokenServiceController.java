package com.leap.shoppingcar.Controller;

import com.leap.shoppingcar.Interface.TokenService;
import com.leap.shoppingcar.Model.objToken;
import com.leap.shoppingcar.Model.resultToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TokenServiceController {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(value = "/api/test/token", method = RequestMethod.POST)
    public resultToken createProducts(@RequestBody objToken token) {

        return restTemplate.exchange("http://187.210.141.12:3001/cliente/token", HttpMethod.POST, tokenService.getToken(token), resultToken.class).getBody();
    }
    
    
}