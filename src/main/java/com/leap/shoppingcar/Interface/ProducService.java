package com.leap.shoppingcar.Interface;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.leap.shoppingcar.Model.ProductDTO.prodDetail;
import com.leap.shoppingcar.Model.ProductDTO.prodSpec;

public interface ProducService {

    void getProductinfo() throws JsonParseException, JsonMappingException, IOException;
    List<prodInterSearch> getSearchProd(String search);
    prodDetail getprod(int idprod);
    List<prodSpec> getprodSpecs(int id);
    
}
