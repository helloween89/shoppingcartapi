package com.leap.shoppingcar.Model.ProductDTO;

import javax.persistence.Column;

public class prodDetail {

    private int id;
    private String clave;
    private String name;
    private String brand;
    private int catid;
    private String cat;
    private String subcat;
    @Column(name="id")
    private int id_detail_prod;
    private double price;
    private String currency;
    private String image;

    public prodDetail(){}

    public prodDetail(int id, String clave, String name, String brand, int catid, String cat, String subcat,int id_detail_prod, double price, String currency, String image){
        this.id=id;
        this.clave=clave;
        this.name=name;
        this.brand=brand;
        this.catid=catid;
        this.cat=cat;
        this.subcat=subcat;
        this.id_detail_prod=id_detail_prod;
        this.price=price;
        this.currency=currency;
        this.image=image;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClave() {
        return this.clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getCatid() {
        return this.catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }

    public String getCat() {
        return this.cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getSubcat() {
        return this.subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    public int getId_detail_prod() {
        return this.id_detail_prod;
    }

    public void setId_detail_prod(int id_detail_prod) {
        this.id_detail_prod = id_detail_prod;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    } 
    
}
