package com.leap.shoppingcar.Model.ProductDB;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "subcat")
public class subcat {

    @Id
    private int id;
    private String namesubcat;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_cat", nullable = false)
    private category cat;
    @OneToMany(mappedBy = "subCat", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL) 
    private Set<product> product;

    public subcat(){}

    public subcat(int id, String namesubcat, category cat){
        this.id=id;
        this.namesubcat=namesubcat;
        this.cat=cat;
    }

    public category getCat() {
        return this.cat;
    }

    public void setCat(category cat) {
        this.cat = cat;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    public String getNamesubcat() {
        return this.namesubcat;
    }

    public void setNamesubcat(String namesubcat) {
        this.namesubcat = namesubcat;
    }
    
}
