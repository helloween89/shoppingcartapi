package com.leap.shoppingcar.Model.ProductDB;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLHStoreType;

import org.hibernate.annotations.TypeDef;

@Entity
@Table(name = "detail_Product")
@TypeDef(name = "hstore", typeClass = PostgreSQLHStoreType.class)
public class detail_product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int active;
    private double price;
    private String currency;
    private String image;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_prod", nullable = false)
    private product prod;
    @OneToMany(mappedBy = "detail_prod_spec", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<product_Spec> prod_spec;
    @OneToMany(mappedBy = "detail_prod_promo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<product_Promo> prod_promo;

    public detail_product() {}

    public detail_product(int active, double price, String currentcy, String image, product prod) {
        this.active=active;
        this.price=price;
        this.currency=currentcy;
        this.image=image;
        this.prod=prod;
    }


    public int getActive() {
        return this.active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public product getProd() {
        return this.prod;
    }

    public void setProd(product prod) {
        this.prod = prod;
    }
    
}
