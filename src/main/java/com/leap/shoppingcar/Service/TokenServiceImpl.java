package com.leap.shoppingcar.Service;

import java.util.Arrays;

import com.leap.shoppingcar.Interface.TokenService;
import com.leap.shoppingcar.Model.objToken;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class TokenServiceImpl implements TokenService {

   @Override
   public HttpEntity<objToken> getToken(@RequestBody objToken product) {
      HttpHeaders headers = new HttpHeaders();
      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
      HttpEntity<objToken> entity = new HttpEntity<objToken>(product,headers);
      return entity;
   }
    
}