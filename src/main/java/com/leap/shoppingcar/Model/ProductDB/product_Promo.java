package com.leap.shoppingcar.Model.ProductDB;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_Promo")
public class product_Promo {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String type;
    private double promotion;
    private String validity_start;
    private String validity_end;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_detail_prod_promo", nullable = false)
    private detail_product detail_prod_promo;
    

    public product_Promo(){}

    public product_Promo(String type, double promotion, String validity_start, String validity_end, detail_product detail_prod_promo){
        this.type=type;
        this.promotion=promotion;
        this.validity_start=validity_start;
        this.validity_end=validity_end;
        this.detail_prod_promo=detail_prod_promo;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPromotion() {
        return this.promotion;
    }

    public void setPromotion(double promotion) {
        this.promotion = promotion;
    }

    public String getValidity_start() {
        return this.validity_start;
    }

    public void setValidity_start(String validity_start) {
        this.validity_start = validity_start;
    }

    public String getValidity_end() {
        return this.validity_end;
    }

    public void setValidity_end(String validity_end) {
        this.validity_end = validity_end;
    }

    public detail_product getDetail_prod() {
        return this.detail_prod_promo;
    }

    public void setDetail_prod(detail_product detail_prod_promo) {
        this.detail_prod_promo = detail_prod_promo;
    }

}
