package com.leap.shoppingcar.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class DAOUser {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	//@Column(columnDefinition="TEXT")
	@Column(length = 40)
	//@JsonIgnore
	private String username;
	//@Column(columnDefinition="TEXT")
	@Column(length = 72)
	//@JsonIgnore
	private String password;
    @Column(length = 20)
	//@JsonIgnore
	private String role;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
    }
	
    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
	}
    
}
