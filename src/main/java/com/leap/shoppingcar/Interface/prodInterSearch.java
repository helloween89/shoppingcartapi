package com.leap.shoppingcar.Interface;

public interface prodInterSearch {
    //@Value("#{target.id}")
    Integer getId();
    //@Value("#{target.clave}")
    String getClave();
    //@Value("#{target.name}")
    String getName();
    //@Value("#{target.brand}")
    String getBrand();
    //@Value("#{target.price}")
    double getPrice();
    //@Value("#{target.currency}")
    String getCurrency();
    //@Value("#{target.image}")
    String getImage();
}
