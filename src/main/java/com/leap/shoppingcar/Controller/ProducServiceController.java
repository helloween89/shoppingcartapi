package com.leap.shoppingcar.Controller;

import java.io.IOException;
import java.util.List;

import com.leap.shoppingcar.Interface.ProducService;
import com.leap.shoppingcar.Interface.prodInterSearch;
import com.leap.shoppingcar.Model.ProductDTO.prodDetail;
import com.leap.shoppingcar.Model.ProductDTO.prodSpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProducServiceController {

    private static final String baseapiurl = "/api/shoppingcart/v1/";

    @Autowired
    private ProducService producService;

    @RequestMapping(value = "/test/getproduct", method = RequestMethod.POST)
    public String createProducts() {
        try {
            producService.getProductinfo();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Controller responded";
    }

    @GetMapping(value = baseapiurl+"search/product/{search}")
    public ResponseEntity<List<prodInterSearch>> getprodSearch(@PathVariable("search") String search) {
        return new ResponseEntity<List<prodInterSearch>>(producService.getSearchProd(search),HttpStatus.OK);
    }

    @GetMapping(value = baseapiurl+"product/{id}")
    public ResponseEntity<prodDetail> getprod(@PathVariable("id") int id){
        return new ResponseEntity<prodDetail>(producService.getprod(id), HttpStatus.OK);
    }

    @GetMapping(value = baseapiurl+"spec_product/{id}")
    public ResponseEntity<List<prodSpec>> getprodspecs(@PathVariable("id") int id){
        return new ResponseEntity<List<prodSpec>>(producService.getprodSpecs(id), HttpStatus.OK);
    }
    
}
