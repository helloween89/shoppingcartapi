package com.leap.shoppingcar.Model.ProductDTO;

public class prodSpec {

    private String type;
    private String value;
    public prodSpec(){}

    public prodSpec(String type, String value){
        this.type=type;
        this.value=value;
    }
    
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
}
