package com.leap.shoppingcar.Exception;

import com.leap.shoppingcar.Model.UserError;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserAlreadyRegisterExceptionController {
   @ExceptionHandler(value = UserAlreadyRegisterException.class)
   public ResponseEntity<Object> exception(UserAlreadyRegisterException exception) {
      UserError userError = new UserError(exception.getMsg(), exception.getCode());
      return new ResponseEntity<>(userError, HttpStatus.OK);
   }
}
