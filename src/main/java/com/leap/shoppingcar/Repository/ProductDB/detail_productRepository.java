package com.leap.shoppingcar.Repository.ProductDB;

import com.leap.shoppingcar.Model.ProductDB.detail_product;
import org.springframework.data.repository.CrudRepository;

public interface detail_productRepository extends CrudRepository<detail_product, Integer> {
    
}
