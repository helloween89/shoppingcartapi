package com.leap.shoppingcar.Model.Product;

public class especificaciones {

    private String tipo;
    private String valor;

    public especificaciones(){}

    public especificaciones(String tipo, String valor){
        this.tipo=tipo;
        this.valor=valor;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return this.valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
