package com.leap.shoppingcar.Model;

public class UserError {

    private String msg;
    private String errcode;

    public UserError(){ }

    public UserError(String msg, String errcode){ 
        this.msg=msg;
        this.errcode=errcode;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErrcode() {
        return this.errcode;
    }

    public void setErrcode(String errcode) {
        this.errcode = errcode;
    }
    
}
