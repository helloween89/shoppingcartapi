package com.leap.shoppingcar.Repository.ProductDB;

import com.leap.shoppingcar.Model.ProductDB.product_Promo;
import org.springframework.data.repository.CrudRepository;

public interface product_PromoRepository extends CrudRepository<product_Promo, Integer> {
    
}
