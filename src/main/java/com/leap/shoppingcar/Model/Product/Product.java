package com.leap.shoppingcar.Model.Product;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

public class Product {

    private int idProducto;
    private String clave;
    private String numParte;
    private String nombre;
    private String modelo;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int idMarca;
    private String marca;
    private int idCategoria;
    private String subcategoria;
    private int protegido;
    private int idSubCategoria;
    private String categoria;
    private String descripcion_corta;
    private String ean;
    @JsonIgnore
    private int upc;
    private String sustituto;
    private int activo;
    private codExistencia existencia;
    private double precio;
    private String moneda;
    private int tipoCambio;
    //Filter especificaciones to avoid null values
    private List<especificaciones> especificaciones;
    //Filter promociones to avoid null values
    private List<objPromociones> promociones;
    private String imagen;

    public List<objPromociones> getPromociones() {
        return this.promociones;
    }

    public void setPromociones(List<objPromociones> promociones) {
        this.promociones = promociones;
    }

    public codExistencia getExistencia() {
        return this.existencia;
    }

    public void setExistencia(codExistencia existencia) {
        this.existencia = existencia;
    }

    public int getIdProducto() {
        return this.idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getClave() {
        return this.clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNumParte() {
        return this.numParte;
    }

    public void setNumParte(String numParte) {
        this.numParte = numParte;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getModelo() {
        return this.modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getIdMarca() {
        return this.idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getIdCategoria() {
        return this.idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getSubcategoria() {
        return this.subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }

    public int getProtegido() {
        return this.protegido;
    }

    public void setProtegido(int protegido) {
        this.protegido = protegido;
    }

    public int getIdSubCategoria() {
        return this.idSubCategoria;
    }

    public void setIdSubCategoria(int idSubCategoria) {
        this.idSubCategoria = idSubCategoria;
    }

    public String getCategoria() {
        return this.categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion_corta() {
        return this.descripcion_corta;
    }

    public void setDescripcion_corta(String descripcion_corta) {
        this.descripcion_corta = descripcion_corta;
    }

    public String getEan() {
        return this.ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public int getUpc() {
        return this.upc;
    }

    public void setUpc(int upc) {
        this.upc = upc;
    }

    public String getSustituto() {
        return this.sustituto;
    }

    public void setSustituto(String sustituto) {
        this.sustituto = sustituto;
    }

    public int getActivo() {
        return this.activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public double getPrecio() {
        return this.precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getMoneda() {
        return this.moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public int getTipoCambio() {
        return this.tipoCambio;
    }

    public void setTipoCambio(int tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public List<especificaciones> getEspecificaciones() {
        return this.especificaciones;
    }

    public void setEspecificaciones(List<especificaciones> especificaciones) {
        this.especificaciones = especificaciones;
    }

    public String getImagen() {
        return this.imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    
    
}
