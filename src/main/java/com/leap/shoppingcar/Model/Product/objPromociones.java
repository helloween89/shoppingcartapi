package com.leap.shoppingcar.Model.Product;

public class objPromociones {

    private String tipo;
    private double promocion;
    private objVigencia vigencia;

    public objPromociones(){}

    public objPromociones(String tipo, double promocion, objVigencia vigencia){
        this.tipo=tipo;
        this.promocion=promocion;
        this.vigencia=vigencia;
    }

    public objVigencia getVigencia() {
        return this.vigencia;
    }

    public void setVigencia(objVigencia vigencia) {
        this.vigencia = vigencia;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getPromocion() {
        return this.promocion;
    }

    public void setPromocion(double promocion) {
        this.promocion = promocion;
    }

}
