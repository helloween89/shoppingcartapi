package com.leap.shoppingcar.Repository.ProductDB;

import com.leap.shoppingcar.Model.ProductDB.product_Spec;
import org.springframework.data.repository.CrudRepository;

public interface product_SpecRepository extends CrudRepository<product_Spec, Integer> {
    
}
