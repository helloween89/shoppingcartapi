package com.leap.shoppingcar.Model.Product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class codExistencia {

    @JsonProperty("LMO")
    private int LMO;
    @JsonProperty("CLN")
    private int CLN;
    @JsonProperty("DGO")
    private int DGO;
    @JsonProperty("TRN")
    private int TRN;
    @JsonProperty("CHI")
    private int CHI;
    @JsonProperty("AGS")
    private int AGS;
    @JsonProperty("QRO")
    private int QRO;
    @JsonProperty("GDL")
    private int GDL;
    @JsonProperty("XLP")
    private int XLP;
    @JsonProperty("COL")
    private int COL;
    @JsonProperty("CTZ")
    private int CTZ;
    @JsonProperty("TAM")
    private int TAM;
    @JsonProperty("VHA")
    private int VHA;
    @JsonProperty("MTY")
    private int MTY;
    @JsonProperty("TPC")
    private int TPC;
    @JsonProperty("OAX")
    private int OAX;
    @JsonProperty("MAZ")
    private int MAZ;
    @JsonProperty("CUE")
    private int CUE;
    @JsonProperty("TOL")
    private int TOL;
    @JsonProperty("PAZ")
    private int PAZ;
    @JsonProperty("CUN")
    private int CUN;
    @JsonProperty("DFP")
    private int DFP;
    @JsonProperty("CJS")
    private int CJS;
    @JsonProperty("ZAC")
    private int ZAC;
    @JsonProperty("IRA")
    private int IRA;
    @JsonProperty("DFC")
    private int DFC;
    @JsonProperty("TXL")
    private int TXL;
    @JsonProperty("CAM")
    private int CAM;
    @JsonProperty("URP")
    private int URP;
    @JsonProperty("CDV")
    private int CDV;
    @JsonProperty("TAP")
    private int TAP;
    @JsonProperty("CEL")
    private int CEL;
    @JsonProperty("D2A")
    private int D2A;

    public int getLMO() {
        return this.LMO;
    }

    public void setLMO(int LMO) {
        this.LMO = LMO;
    }

    public int getCLN() {
        return this.CLN;
    }

    public void setCLN(int CLN) {
        this.CLN = CLN;
    }

    public int getDGO() {
        return this.DGO;
    }

    public void setDGO(int DGO) {
        this.DGO = DGO;
    }

    public int getTRN() {
        return this.TRN;
    }

    public void setTRN(int TRN) {
        this.TRN = TRN;
    }

    public int getCHI() {
        return this.CHI;
    }

    public void setCHI(int CHI) {
        this.CHI = CHI;
    }

    public int getAGS() {
        return this.AGS;
    }

    public void setAGS(int AGS) {
        this.AGS = AGS;
    }

    public int getQRO() {
        return this.QRO;
    }

    public void setQRO(int QRO) {
        this.QRO = QRO;
    }

    public int getGDL() {
        return this.GDL;
    }

    public void setGDL(int GDL) {
        this.GDL = GDL;
    }

    public int getXLP() {
        return this.XLP;
    }

    public void setXLP(int XLP) {
        this.XLP = XLP;
    }

    public int getCOL() {
        return this.COL;
    }

    public void setCOL(int COL) {
        this.COL = COL;
    }

    public int getCTZ() {
        return this.CTZ;
    }

    public void setCTZ(int CTZ) {
        this.CTZ = CTZ;
    }

    public int getTAM() {
        return this.TAM;
    }

    public void setTAM(int TAM) {
        this.TAM = TAM;
    }

    public int getVHA() {
        return this.VHA;
    }

    public void setVHA(int VHA) {
        this.VHA = VHA;
    }

    public int getMTY() {
        return this.MTY;
    }

    public void setMTY(int MTY) {
        this.MTY = MTY;
    }

    public int getTPC() {
        return this.TPC;
    }

    public void setTPC(int TPC) {
        this.TPC = TPC;
    }

    public int getOAX() {
        return this.OAX;
    }

    public void setOAX(int OAX) {
        this.OAX = OAX;
    }

    public int getMAZ() {
        return this.MAZ;
    }

    public void setMAZ(int MAZ) {
        this.MAZ = MAZ;
    }

    public int getCUE() {
        return this.CUE;
    }

    public void setCUE(int CUE) {
        this.CUE = CUE;
    }

    public int getTOL() {
        return this.TOL;
    }

    public void setTOL(int TOL) {
        this.TOL = TOL;
    }

    public int getPAZ() {
        return this.PAZ;
    }

    public void setPAZ(int PAZ) {
        this.PAZ = PAZ;
    }

    public int getCUN() {
        return this.CUN;
    }

    public void setCUN(int CUN) {
        this.CUN = CUN;
    }

    public int getDFP() {
        return this.DFP;
    }

    public void setDFP(int DFP) {
        this.DFP = DFP;
    }

    public int getCJS() {
        return this.CJS;
    }

    public void setCJS(int CJS) {
        this.CJS = CJS;
    }

    public int getZAC() {
        return this.ZAC;
    }

    public void setZAC(int ZAC) {
        this.ZAC = ZAC;
    }

    public int getIRA() {
        return this.IRA;
    }

    public void setIRA(int IRA) {
        this.IRA = IRA;
    }

    public int getDFC() {
        return this.DFC;
    }

    public void setDFC(int DFC) {
        this.DFC = DFC;
    }

    public int getTXL() {
        return this.TXL;
    }

    public void setTXL(int TXL) {
        this.TXL = TXL;
    }

    public int getCAM() {
        return this.CAM;
    }

    public void setCAM(int CAM) {
        this.CAM = CAM;
    }

    public int getURP() {
        return this.URP;
    }

    public void setURP(int URP) {
        this.URP = URP;
    }

    public int getCDV() {
        return this.CDV;
    }

    public void setCDV(int CDV) {
        this.CDV = CDV;
    }

    public int getTAP() {
        return this.TAP;
    }

    public void setTAP(int TAP) {
        this.TAP = TAP;
    }

    public int getCEL() {
        return this.CEL;
    }

    public void setCEL(int CEL) {
        this.CEL = CEL;
    }

    public int getD2A() {
        return this.D2A;
    }

    public void setD2A(int D2A) {
        this.D2A = D2A;
    }
    
}
