package com.leap.shoppingcar.Repository.ProductDB;

import com.leap.shoppingcar.Model.ProductDB.subcat;
import org.springframework.data.repository.CrudRepository;

public interface subcatRepository extends CrudRepository<subcat, Integer> {
    
}
