package com.leap.shoppingcar.Repository.ProductDB;

import com.leap.shoppingcar.Model.ProductDB.brand;

import org.springframework.data.repository.CrudRepository;

public interface brandRepository extends CrudRepository<brand, Integer> {
    
}
