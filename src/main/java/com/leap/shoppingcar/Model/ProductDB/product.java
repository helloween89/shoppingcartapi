package com.leap.shoppingcar.Model.ProductDB;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class product {

    @Id
    private int id;
    private String clave;
    private String name;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_cat", nullable = false)
    private category cat;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_subcat", nullable = false)
    private subcat subCat;
    @OneToMany(mappedBy = "prod", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private Set<detail_product> detailprod;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_brand", nullable = false)
    private brand brandprod;

    public product(){}

    public product(int id, String clave, String name, category cat, subcat subCat, brand brandprod){
        this.id=id;
        this.clave=clave;
        this.name=name;
        this.cat=cat;
        this.subCat=subCat;
        this.brandprod=brandprod;
    }

    public category getCat() {
        return this.cat;
    }

    public void setCat(category cat) {
        this.cat = cat;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public subcat getSubCat() {
        return this.subCat;
    }

    public void setSubCat(subcat subCat) {
        this.subCat = subCat;
    }

    public String getClave() {
        return this.clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
}
