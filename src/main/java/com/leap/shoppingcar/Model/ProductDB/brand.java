package com.leap.shoppingcar.Model.ProductDB;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "brand")
public class brand {

    @Id
    private int id;
    private String brand;
    @OneToMany(mappedBy = "brandprod", fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    private Set<product> prod;

    public brand(){

    }

    public brand(int id, String brand){
        this.id=id;
        this.brand=brand;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }


    
}
