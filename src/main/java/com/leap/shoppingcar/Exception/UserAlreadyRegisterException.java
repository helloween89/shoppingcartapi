package com.leap.shoppingcar.Exception;

public class UserAlreadyRegisterException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String code;
    private String msg;

    public UserAlreadyRegisterException(){ }

    public UserAlreadyRegisterException(String code, String msg){ 
        this.code=code;
        this.msg=msg;
    }
    
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

 }
