package com.leap.shoppingcar.Repository;

import com.leap.shoppingcar.Model.DAOUser;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<DAOUser, Integer> {
    DAOUser findByUsername(String username);
}
