package com.leap.shoppingcar.Model.ProductDB;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_Spec")
public class product_Spec {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String type;
    private String value;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_detail_prod_spec", nullable = false)
    private detail_product detail_prod_spec;

    public product_Spec(){}

    public product_Spec(String type, String value, detail_product detail_prod_spec){
        this.type=type;
        this.value=value;
        this.detail_prod_spec=detail_prod_spec;
    }

    public detail_product getProd_spec() {
        return this.detail_prod_spec;
    }

    public void setProd_spec(detail_product detail_prod_spec) {
        this.detail_prod_spec = detail_prod_spec;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
