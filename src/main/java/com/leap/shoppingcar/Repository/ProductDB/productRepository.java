package com.leap.shoppingcar.Repository.ProductDB;

import java.util.List;

import com.leap.shoppingcar.Interface.prodInterSearch;
import com.leap.shoppingcar.Model.ProductDB.product;
import com.leap.shoppingcar.Model.ProductDTO.prodDetail;
import com.leap.shoppingcar.Model.ProductDTO.prodSpec;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface productRepository extends CrudRepository<product, Integer> {

    @Query(value = "select p.id as id, p.clave as clave, p.name as name, b.brand as brand, dp.price as price, dp.currency as currency, dp.image as image"
    +" from product p"
    +" inner join detail_product dp on dp.id_prod = p.id"
    +" inner join brand b on b.id = p.id_brand"
    +" where name ilike :search% and dp.active = 1", nativeQuery = true)
    List<prodInterSearch> findsearchprod(@Param("search") String search);

    @Query(value = "select new com.leap.shoppingcar.Model.ProductDTO.prodDetail(p.id as id, p.clave as clave, p.name as name, b.brand as brand, c.id as catid, c.namecat as cat, sub.namesubcat as subcat, dp.id as id_detail_prod, dp.price as price, dp.currency as currency, dp.image as image)"
    +" from product as p"
    +" inner join p.detailprod as dp"
    +" inner join p.brandprod as b"
    +" inner join p.cat as c"
    +" inner join p.subCat as sub"
    +" where p.id = :id")
    prodDetail findprod(@Param("id") int id);

    @Query("select new com.leap.shoppingcar.Model.ProductDTO.prodSpec(ps.type, ps.value)"
    +" from detail_product dp"
    +" inner join dp.prod_spec as ps"
    +" where dp.id = :id")
    List<prodSpec> finprodspec(@Param("id") int id);

}
