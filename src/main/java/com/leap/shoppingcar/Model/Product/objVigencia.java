package com.leap.shoppingcar.Model.Product;

public class objVigencia {

    private String inicio;
    private String fin;

    public objVigencia(){}

    public objVigencia(String inicio, String fin){
        this.inicio=inicio;
        this.fin=fin;
    }

    public String getInicio() {
        return this.inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return this.fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }
    
}
