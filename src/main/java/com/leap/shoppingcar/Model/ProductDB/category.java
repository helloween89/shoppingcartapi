package com.leap.shoppingcar.Model.ProductDB;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class category {

    @Id
    private int id;
    private String namecat; 
    @OneToMany(mappedBy = "cat", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL) 
    private Set<subcat> subcat;
    @OneToMany(mappedBy = "cat", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL) 
    private Set<product> product;

    public category(){}

    public category(int id, String namecat){
        this.id=id;
        this.namecat=namecat;
    }

    public Set<subcat> getSubcat() {
        return this.subcat;
    }

    public void setSubcat(Set<subcat> subcat) {
        this.subcat = subcat;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamecat() {
        return this.namecat;
    }

    public void setNamecat(String namecat) {
        this.namecat = namecat;
    }
    
}
