package com.leap.shoppingcar.Model;

public class resultToken {
    
    private String token;
    private String time;

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}